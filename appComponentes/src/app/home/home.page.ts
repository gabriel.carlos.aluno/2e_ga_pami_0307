import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "CriminalTV";
  series = [
             {
                  titulo: "Miami Vice",
                  ano:'1984',
                  capa:"https://www.maxicar.com.br/wp-content/uploads/2021/09/Ferrari-Daytona-Miami-Vice-990x557.jpg",
                  texto:"Resplandecente com a autêntica música, moda e atmosfera da década de 1980, Miami Vice segue dois detetives disfarçados e sua equipe espalhada pelas ruas de Miami"
             },
             {​
              titulo: 'CSI: Miami',
              ano: '2002',
              capa: 'https://flxt.tmsimg.com/assets/p184820_i_h10_ae.jpg',
              texto: "É uma série de televisão americana que mostrou o trabalho de investigação criminal de uma equipe em Miami. O seriado foi o primeiro spin-off e segunda série da franquia CSI: Crime Scene Investigation."
            }​,
            {​
              titulo: "Brooklyn Nine-Nine",
              ano: '2013',
              capa: "https://streamingsbrasil.com/wp-content/uploads/2022/01/Brooklyn-99-Temp-9-Thumbnail-1130x580.jpg",
              texto: "Jake Peralta, um detetive imaturo de Nova York, entra em conflito com seu novo comandante, o sério e severo Capitão Ray Holt."
            }​,
            {​
              titulo: "Criminal Minds",
              ano: '2005',
              capa: "https://multiversonoticias.com.br/wp-content/uploads/2022/02/multiverso-2.jpg",
              texto: "Os casos da unidade de análise de comportamento do FBI, um grupo que estuda aos assassinos mais perigosos dos Estados Unidos, antes do próximo ataque deles."
            }​           
          ];
  constructor() {}

}
