import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalhes-serie',
  templateUrl: './detalhes-serie.page.html',
  styleUrls: ['./detalhes-serie.page.scss'],
})
export class DetalhesSeriePage implements OnInit {
    titulo = "CriminalTV";
    serie = {
              titulo: "Miami Vice",
              detalhes:'1984 ∘ 5 Temporadas ∘ 18 ∘ Imdb: ★7,6/10 ',
              capa:"https://www.maxicar.com.br/wp-content/uploads/2021/09/Ferrari-Daytona-Miami-Vice-990x557.jpg",
              texto:"Resplandecente com a autêntica música, moda e atmosfera da década de 1980, Miami Vice segue dois detetives disfarçados e sua equipe espalhada pelas ruas de Miami",
              play:"https://freepikpsd.com/file/2020/01/Play-Button-PNG-Transparent-Image.png"
            }

  constructor() { }

  ngOnInit() {
  }

}
